// ---------------------------------------------------------------
// shaderapidx10.qpc
// ---------------------------------------------------------------
macro PROJECT_NAME "shaderapidx10"

macro SRC_DIR "../.."
macro OUT_BIN_DIR "$SRC_DIR/../game/bin"

include "$SRC_DIR/_qpc_scripts/source_dll_base.qpc"

configuration
{
	general
	{
		out_dir     "./$CONFIG_dx10$PLAT_DIR"
		build_dir   "./$CONFIG_dx10$PLAT_DIR"
		
		include_directories
		{
			"$SRC_DIR/dx10sdk/include"
			"../"
		}
	}
	
	compiler
	{
		preprocessor_definitions
		{
			"USE_ACTUAL_DX" [$WINDOWS || $X360]
			"SHADERAPIDX10"
			"SHADER_DLL_EXPORT"
			"PROTECTED_THINGS_ENABLE"
			"strncpy=use_Q_strncpy_instead"
			"_snprintf=use_Q_snprintf_instead"
		}
	}
	
	linker
	{
		libraries
		{
			"$LIBCOMMON/vc7/bzip2" [!$VS2015]
			"$LIBCOMMON/vc14/bzip2" [$VS2015]
			"$LIBPUBLIC/tier2"
			"$LIBPUBLIC/bitmap"
			"$LIBPUBLIC/mathlib"
			"$LIBPUBLIC/videocfg"
			"$SRC_DIR/dx10sdk/lib/x86/d3d9"
			"$SRC_DIR/dx10sdk/lib/x86/d3d10"
			"$SRC_DIR/dx10sdk/lib/x86/dxgi"
			"$SRC_DIR/dx10sdk/lib/x86/d3dx10.lib $SRC_DIR/dx10sdk/lib/x86/d3dx9"
			"$SRC_DIR/dx10sdk/lib/x86/d3dx10d.lib $SRC_DIR/dx10sdk/lib/x86/d3dx9d"
		}
	}
}

dependencies
{
	"tier2"
	"bitmap"
	"mathlib"
	"bzip2"	[$VS2015]
	"videocfg"
}

files
{
	folder "Source Files"
	{
		"cvballoctracker.cpp"
		"shaderdevicebase.cpp"
		"shaderapibase.cpp"
		"meshbase.cpp"

		"ShaderDeviceDx10.cpp"	\
		"ShaderAPIDx10.cpp"	\
		"MeshDx10.cpp"	\
		"InputLayoutDx10.cpp"	\
		"ShaderShadowDx10.cpp"
		{
			preprocessor_definitions
			{
				"DX10"
			}
		}

		"ColorFormatDX8.cpp"
		"d3d_async.cpp"
		"$SRC_DIR/public/filesystem_helpers.cpp"
		"HardwareConfig.cpp"
		"MeshDX8.cpp"
		"Recording.cpp"
		"ShaderAPIDX8.cpp"
		"ShaderDeviceDX8.cpp"
		"ShaderShadowDX8.cpp"
		"TextureDX8.cpp"
		"TransitionTable.cpp"
		"vertexdecl.cpp"
		"VertexShaderDX8.cpp"
		"wmi.cpp"
		"dx9asmtogl2.cpp" [!$PS3]
	}

	folder "DirectX Header Files"
	{
		"$SRC_DIR/dx10sdk/include/d3d10.h"
		"$SRC_DIR/dx10sdk/include/d3dx10.h"
		"$SRC_DIR/dx10sdk/include/d3dx10core.h"
		"$SRC_DIR/dx10sdk/include/d3dx10math.h"
		"$SRC_DIR/dx10sdk/include/d3dx10math.inl"
		"$SRC_DIR/dx10sdk/include/d3dx10mesh.h"
		"$SRC_DIR/dx10sdk/include/d3dx10tex.h"
	}

	folder "Public Header Files"
	{
		"$SRC_DIR/public/shaderapi/ishaderdevice.h"
		"$SRC_DIR/public/shaderapi/ishaderutil.h"
		"$SRC_DIR/public/shaderapi/ishaderapi.h"
		"$SRC_DIR/public/shaderapi/ishaderdynamic.h"
		"$SRC_DIR/public/shaderapi/ishadershadow.h"
		"$SRC_DIR/public/materialsystem/idebugtextureinfo.h"
		"$SRC_DIR/public/materialsystem/ivballoctracker.h"
		"$SRC_DIR/public/materialsystem/shader_vcs_version.h"
	}

	folder "Header Files"
	{
		"meshbase.h"
		"shaderdevicebase.h"
		"shaderapibase.h"
		"shaderapi_global.h"
		"HardwareConfig.h"
		"ShaderDeviceDx10.h"
		"ShaderAPIDx10.h"
		"MeshDx10.h"
		"ShaderShadowDx10.h"
		"shaderapidx10_global.h"
		"inputlayoutdx10.h"
		"TransitionTable.h"
		"vertexdecl.h"
		"CMaterialSystemStats.h"
		"ColorFormatDX8.h"
		"d3d_async.h"
		"dynamicib.h"
		"dynamicvb.h"
		"IMeshDX8.h"
		"locald3dtypes.h"
		"Recording.h"
		"ShaderAPIDX8.h"
		"ShaderAPIDX8_Global.h"
		"ShaderShadowDX8.h"
		"stubd3ddevice.h"
		"TextureDX8.h"
		"VertexShaderDX8.h"
		"wmi.h"
	}
}
