import os
import sys
import subprocess
import hash_storage


SCRIPT_PATH = os.path.split(os.path.abspath(__file__))[0]
HASH_FILE = SCRIPT_PATH + "/tofile_hashes.txt"


def main():
    output_file = sys.argv[1]
    command = sys.argv[2:]

    # if hash_storage.check_hash(HASH_FILE, output_file):
    #     print("Protobuf hash is valid: \"" + args.input + "\"")
    #     return
    
    output = subprocess.check_output(" ".join(command), shell=True)
    output = output.split(b"\r\n", 1)[1]  # idk why this happens
    
    if os.path.dirname(output_file) != "":
        if not os.path.exists(os.path.dirname(output_file)):
            os.makedirs(os.path.dirname(output_file))
    
    with open(output_file, "wb") as output_io:
        output_io.write(output)
        
    print(f"Wrote \"{output_file}\"")


if __name__ == "__main__":
    main()
