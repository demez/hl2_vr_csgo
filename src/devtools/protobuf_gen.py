import os
import sys
import subprocess
import argparse
import hash_storage


def parse_args() -> argparse.Namespace:
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--src", "-s")
    arg_parser.add_argument("--input", "-i")
    arg_parser.add_argument("--output", "-o")
    # arg_parser.add_argument("--paths", "-p", nargs="+")
    arg_parser.add_argument("--force", "-f", action="store_true")
    return arg_parser.parse_args()


SCRIPT_PATH = os.path.split(os.path.abspath(__file__))[0]
HASH_FILE = SCRIPT_PATH + "/protobuf_hashes.txt"


def main():
    args = parse_args()

    if sys.platform == "win32":
        protoc_exe = "/devtools/bin/protoc.exe"
    elif sys.platform.startswith("linux"):
        protoc_exe = "/devtools/bin/linux/protoc"
    elif sys.platform == "darwin":
        protoc_exe = "/devtools/bin/osx32/protoc"
    else:
        print("what")
        return
    
    protbuf_dir = os.path.dirname(args.input)
    protbuf_name = os.path.splitext(os.path.basename(args.input))[0]
    protbuf_ext = os.path.splitext(os.path.basename(args.input))[1]
    
    output_file = args.output + "/" + protbuf_name + ".pb.cc"

    # hash_folder = SCRIPT_PATH + "/protobuf_hashes/"
    # if not os.path.exists(hash_folder):
    #     os.makedirs(hash_folder)
        
    # hash_file = hash_folder + protbuf_name + protbuf_ext + ".hash"
    
    if not args.force:
        if os.path.isfile(output_file):
            if hash_storage.check_hash(HASH_FILE, args.input, output_file):
                print("Protobuf hash valid: \"" + args.input + "\"")
                return
        
        # if not os.path.isfile(output_file):
        #     print("Protobuf file already made: \"" + args.input + "\"")
        #     hash_storage.update_hash(HASH_FILE, args.input, output_file)
        #     return
        
    if not os.path.exists(args.output):
        os.makedirs(args.output)
    
    cmd = [
        args.src + protoc_exe,
        f"\"--proto_path={args.src}/thirdparty/protobuf-2.5.0/src\"",
        f"\"--proto_path={protbuf_dir}\"",
        f"\"--proto_path={args.src}/gcsdk\"",
        f"\"--proto_path={args.src}/common\"",
        f"\"--proto_path={args.src}/game/shared\"",
        
        # dumb
        f"\"--proto_path={args.src}/game/shared/cstrike15\"",
        # f"\"--proto_path={args.src}/game/shared/protobuf\"",
        
        f"\"--cpp_out={args.output}\"",
        f"\"{args.input}\"",
    ]
    
    # --cpp_out=$GENERATED_PROTO_DIR $PROTOBUF_DIR/$PROTOBUF_FILE_$EXT"

    # subprocess.check_output()
    output = subprocess.run(" ".join(cmd), shell=True)
    
    if output.returncode != 0:
        print("Failed to compile protobuf file: \"" + args.input + "\"")
        quit(1)
    else:
        print("Protobuf file created: \"" + output_file + "\"")
        hash_storage.update_hash(HASH_FILE, args.input, output_file)
    
    # if not os.path.isfile(output_file):
    #     print("Failed to compile protobuf file: \"" + args.input + "\"")
    #     quit(1)
    # else:
    #     print("Protobuf file created: \"" + output_file + "\"")


if __name__ == "__main__":
    main()



